<html>

<head>
<title>Unit Converter - Speed</title>
</head>

<body>
<form action="speed.php" method="POST">
<h1 align=center><font color=blue><ins>SPEED CONVERSION</ins></font></h1>
<h1 align="right"><font size="3" color="#FF0000"><a href="../index.html"><ins>home</ins></a></font></h1>
<table align=center>
<tr>
<td>Enter Value </td>
<td><input type=text name=val></td>
</tr>
<tr>
<td>From</td>
<td>
<select name="from">
            <option value=0>--select--</option>
            <option value=1>Kilometers per hour</option>
            <option value=2>Miles per hour</option>
            <option value=3>Kilometers per second</option>
            <option value=4>Meters per second</option>
            <option value=5>Knot</option>
            <option value=6>Mach</option>

</select>
</td>
</tr>
</table>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=submit value=Convert></p>

<?php
if(isset($_POST['val']))
{
$val=$_POST['val'];
if(!preg_match('/^[0-9.]/',$val))
{
 echo '<script language="JavaScript">'."\n".'alert("Invalid input");'."\n";
 echo "window.location=('speed.php');\n";
 echo '</script>';
}
$from=$_POST['from'];
if($from==0)
{
echo '<script language="JavaScript">'."\n".'alert("Please select a base unit");'."\n";
echo '</script>';
}
else
{
function assign($from,$val)
{
switch ($from)
{
case 1:
$fromu="Kilometers per hour";$kmh=1;$mph=0.621371;$kms=0.000278;$mps=0.277778;$kn=0.539957;$ma=0.000816;break;
case 2:
$fromu="Miles per hour";$kmh=1.609344;$mph=1;$kms=0.000447;$mps=0.447040;$kn=0.868976;$ma=0.001314;break;
case 3:
$fromu="Kilometers per second";$kmh=3600;$mph=2236.936292;$kms=1;$mps=1000;$kn=1943.844492;$ma=2.938776;break;
case 4:
$fromu="Meters per second";$kmh=3.6;$mph=2.236936;$kms=0.001;$mps=1;$kn=1.943844;$ma=0.002939;break;
case 5:
$fromu="Knot";$kmh=1.852;$mph=1.150779;$kms=0.000514;$mps=0.514444;$kn=1;$ma=0.001512;break;
case 6:
$fromu="Mach";$kmh=1225;$mph=161.17971;$kms=0.340278;$mps=340.277778;$kn=661.447084;$ma=1;break;
}
 echo "<br><br><table align=center>
 <tr><td><h3> ",$val," ",$fromu," equivalent is </h3></td></tr>
 <tr align=right><td><u> ",(double)($val*$kmh),"</u> Kilometers per hour</td></tr>
 <tr align=right><td><u> ",(double)($val*$mph),"</u> Meters per hour</td></tr>
 <tr align=right><td><u> ",(double)($val*$kms),"</u> kilometers per second</td></tr>
 <tr align=right><td><u> ",(double)($val*$mps),"</u> Meters per second</td></tr>
 <tr align=right><td><u> ",(double)($val*$kn),"</u> Knots</td></tr>
 <tr align=right><td><u> ",(double)($val*$ma),"</u> MACH</td></tr>

</table>";
}
assign($from,$val);
}
}
?>
</form>
</body>

</html>
