<html>

<head>
<title>Unit Converter - Pressure</title>
</head>

<body>
<form action="pressure.php" method="POST">
<h1 align=center><font color=blue><ins>PRESSURE CONVERSION</ins></font></h1>
<h1 align="right"><font size="3" color="#FF0000"><a href="../index.html"><ins>home</ins></a></font></h1>
<table align=center>
<tr>
<td>Enter Value </td>
<td><input type=text name=val></td>
</tr>
<tr>
<td>From</td>
<td>
<select name="from">
            <option value=0>--select--</option>
            <option value=1>Pascal</option>
            <option value=2>Bar</option>
            <option value=3>PSI</option>
            <option value=4>HG mm</option>
            <option value=5>Kilogram per square meter</option>

</select>
</td>
</tr>
</table>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=submit value=Convert></p>

<?php
if(isset($_POST['val']))
{
$val=$_POST['val'];
if(!preg_match('/^[0-9.]/',$val))
{
 echo '<script language="JavaScript">'."\n".'alert("Invalid input");'."\n";
 echo "window.location=('pressure.php');\n";
 echo '</script>';
}
$from=$_POST['from'];
if($from==0)
{
echo '<script language="JavaScript">'."\n".'alert("Please select a base unit");'."\n";
echo '</script>';
}
else
{
function assign($from,$val)
{
switch ($from)
{
case 1:
$fromu="PASCAL";$pa=1;$bar=0.00001;$psi=0.0001;$hg=0.0075;$km=0.102;break;
case 2:
$fromu="BAR";$pa=100000;$bar=1;$psi=14.503768;$hg=750.063755;$km=10197.1621;break;
case 3:
$fromu="PSI";$pa=6894.76;$bar=0.068948;$psi=1;$hg=51.715096;$km=703.069856;break;
case 4:
$fromu="HG MM";$pa=133.322;$bar=0.001333;$psi=0.019337;$hg=1;$km=13.59506;break;
case 5:
$fromu="KG/M^2";$pa=9.80665;$bar=0.000098;$psi=0.001422;$hg=0.073556;$km=1;break;
}
 echo "<br><br><table align=center>
 <tr><td><h3> ",$val," ",$fromu," equivalent is </h3></td></tr>
 <tr align=right><td><u> ",(double)($val*$pa),"</u> PASCAL</td></tr>
 <tr align=right><td><u> ",(double)($val*$bar),"</u> BAR</td></tr>
 <tr align=right><td><u> ",(double)($val*$psi),"</u> PSI</td></tr>
 <tr align=right><td><u> ",(double)($val*$hg),"</u> HG mm</td></tr>
 <tr align=right><td><u> ",(double)($val*$km),"</u> Kilogram per square meter</td></tr>
</table>";
}
assign($from,$val);
}
}
?>
</form>
</body>

</html>
